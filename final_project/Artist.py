class Artist:
    """
    Артист театра
    """

    def __init__(self, first_name, last_name, age, group, category, level):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.group = group  # солист, хорист, танцор, или дирижер
        self.category = category  # Артист мюзикла или оперетты
        self.level = level

    def __str__(self):
        return f"first_name: {self.first_name} | last_name: {self.last_name} | age: {self.age} | group: {self.group} |" \
               f"category: {self.category}, | level: {self.level}"

