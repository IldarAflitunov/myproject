from final_project.Artist import Artist


class ArtistOfSolo(Artist):
    def __init__(self, type_of_the_voice, role, first_name, last_name, age, group, category, level):
        super().__init__(first_name, last_name, age, group, category, level)
        self.type_of_the_voice = type_of_the_voice
        self.role = role

    def __str__(self):
        return f"ArtistOfSolo({super().__str__()}| type_of_the_voice: {self.type_of_the_voice} " \
               f"| role :{self.role}"


if __name__ == '__main__':
    solist = ArtistOfSolo(first_name='Inokenty', last_name='Dobryh', age=35, group='Solist',
                          category='Musicle', level ='Highest')
    #  print(solist)
