from final_project.Artist import Artist


class ArtistOfTheChoir(Artist):
    def __init__(self, type_of_the_voice, level, first_name, last_name, age, group, category):
        super().__init__(first_name, last_name, age, group, category, level)
        self.type_of_the_voice = type_of_the_voice

    def __str__(self):
        return f"ArtistOfTheChoir({super.__str__()}| type_of_the_voice : {self.type_of_the_voice }"


