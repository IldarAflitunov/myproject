from final_project.Artist import Artist


class ArtistOfTheOrchestra(Artist):
    def __init__(self, group_of_instruments, instrument, first_name, last_name, age, group, category, level):
        super().__init__(first_name, last_name, age, group, category, level)
        self.group_of_instruments = group_of_instruments
        self.instrument = instrument

    def __str__(self):
        return f"ArtistOfTheOrchestra ({super().__str__()}|group_of_instruments: {self.group_of_instruments}" \
               f"|instrument : {self.instrument} "


