from final_project.Artist import Artist


class Conductor(Artist):
    def __init__(self, specialisation, first_name, last_name, age, group, category, level):
        super().__init__(first_name, last_name, age, group, category, level)
        self.specialisation = specialisation

    def __str__(self):
        return f"Conductor ({super().__str__()} |specialisation : {self.specialisation}"


