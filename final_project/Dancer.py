from final_project.Artist import Artist

"""
Танцор
"""


class Dancer(Artist):
    def __init__(self, gender, first_name, last_name, age, group, category, level):
        super().__init__(first_name, last_name, age, group, category)
        self.gender = gender

    def __str__(self):
        return f"Dancer({super().__str__()}| gender : {self.gender}"
