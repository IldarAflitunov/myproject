class Theatre:
    """
    Театр
    """

    def __init__(self, name):
        self.name = name

    def __str__(self):
        return f"name: {self.name}"
