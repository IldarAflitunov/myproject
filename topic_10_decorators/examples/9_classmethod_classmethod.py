class MyClass:
    class_state = '234'     # поле класса

    def __init__(self):
        self.obj_state = '123'  # поле экземпляра

    def instancemethod(self):   # метод экзепляра
        return 'instance method called', self.class_state, self.__class__.class_state, self.obj_state, self

    def change_class_state(self, string):   # метод экзепляра
        # инициализация (создание и присвоение значения) поля экземпляра (с таким же именем как и поле класса)
        self.class_state += string

        # изменение значения поля класса
        self.__class__.class_state += string*2

    @classmethod
    def classmethod(cls):   # метод класса
        return 'class method called', cls.class_state, cls

    @staticmethod
    def staticmethod(my_class):     # статический метод
        return 'static method called', my_class.class_state


obj = MyClass()
obj.change_class_state('q')
print(obj.instancemethod())         # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
print(MyClass.instancemethod(obj))  # ('instance method called', <__main__.MyClass object at 0x7f36bbd98a10>)
# print(MyClass.instancemethod())   # TypeError: instancemethod() missing 1 required positional argument: 'self'

print(obj.classmethod())            # ('class method called', <class '__main__.MyClass'>)
print(MyClass.classmethod())        # ('class method called', <class '__main__.MyClass'>)

print(obj.staticmethod(obj))           # static method called
print(MyClass.staticmethod(obj))       # static method called
# ---------------------------------------------------------------------------------------------------------------------

print("welcome".upper())  # метод экземпляра класса

print(dict.fromkeys('AEIOU'))  # метод класса dict
