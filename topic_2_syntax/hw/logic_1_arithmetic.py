def arithmetic(arg1: int, arg2: int, operation: str):
    #   arg1 = 0
    #   arg2 = 0
    #   operation = " "
    # с объявленными переменными впадает п случай неизвестного оператора. Почему?

    #    if not arg1.isdigit() or arg1.isdigit() :
    #        return 'oops'  надо почитать про преобразование типов в питоне

    if operation == "+":
        return arg1 + arg2
    elif operation == "-":
        return arg1 - arg2
    elif operation == "*":
        return arg1 * arg2
    elif operation == "/":
        return arg1 / arg2
    else:
        return "Unknown operator"

    """
    Функция arithmetic.
    
    Принимает 3 аргумента: первые 2 - числа, третий - операция, которая должна быть произведена над ними.
    Если третий аргумент +, сложить их;
    если —, то вычесть;
    если *, то умножить;
    если /, то разделить (первое на второе).
    В остальных случаях вернуть строку "Unknown operator".
    Вернуть результат операции.
    """


def main():
    print(arithmetic(3, 6, '/'))
    print(arithmetic(6, 5674, '*'))
    print(arithmetic(-6, 5674, '+'))
    print(arithmetic(6, 5674, '-'))

    # a = input("a= ")
    # b = input("b= ")
    # operation = input("operation is ")

    #   operation = int(input("operation is "))  приведение к инт или стр не сработало
    #     Traceback(most
    #     recent
    #     call
    #     last):
    #     File
    #     "C:/Users/Ильдар/PycharmProjects/myproject/topic_2_syntax/hw/logic_1_arithmetic.py", line   47, in < module >
    #     main()
    #
    #
    # File
    # "C:/Users/Ильдар/PycharmProjects/myproject/topic_2_syntax/hw/logic_1_arithmetic.py", line 42, in main
    # operation = int(input("operation is ")) ValueError: invalid literal for int() with base 10: '*'

    # print(arithmetic(a, b, operation))


if __name__ == '__main__':
    main()
