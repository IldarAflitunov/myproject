def if_3_do(arg):
    if arg > 3:
        return arg + 10
    else:
        return arg - 10

    """
    Функция if_3_do.
    
    Принимает число.
    Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
    Вернуть результат.
    """


def main():
    print(if_3_do(3))
    print(if_3_do(6))


if __name__ == '__main__':
    main()
