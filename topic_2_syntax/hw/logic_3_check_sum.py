def check_sum(a, b, c):
    # version 1
    # if a + b == c:
    #     return True
    # elif a + c == b:
    #     return True
    # elif b + c == a:
    #     return True
    # else:
    #     return False

    # version 2
    # if a + b == c or a + c == b or b + c == a:
    #     return True
    # else:
    #     return False

    if abs(b - c) == a or b + c == a:
        return True
    else:
        return False


"""
Функция check_sum.

Принимает 3 числа.
Вернуть True, если можно взять какие-то два из них и в сумме получить третье, иначе False
"""


def main():
    print(check_sum(3, 5, 8))
    print(check_sum(6, 12, 6))
    print(check_sum(6, 12, 7))
    print(check_sum(6, 12, 87))


if __name__ == '__main__':
    main()
