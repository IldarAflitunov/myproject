def print_hi(n):
    # version 1
    # print("Hi, Friend!"*n)

    # version 2
    i = 0
    while i < n:
        print("Hi, friend! ", end='')
        i += 1
    print("")
    #
    # """
    # Функция print_hi.
    #
    # Принимает число n.
    # Выведите на экран n раз фразу "Hi, friend!"
    # Пример: n=3, тогда в результате "Hi, friend!Hi, friend!Hi, friend!"
    # """


def main():
    print_hi(3)
    print_hi(6)
    print_hi(12)
    print_hi(6)


if __name__ == '__main__':
    main()
