def sum_1_112_3():
    a = 1
    sum_a = 1
    while (1 + 3 * a) <= 112:
        sum_a += 1 + 3 * a
        a += 1
    return sum_a


"""
Функция sum_1_112_3.

Вернуть сумму 1+4+7+10+...109+112.
"""


def main():
    print(sum_1_112_3())


if __name__ == '__main__':
    main()
