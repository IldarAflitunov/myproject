def count_odd_num(a):
    if type(a) == int:
        if  a < 0:
            return 'Must be > 0!'
        elif a > 0:
            digits = list(map(int, str(a)))
            counter = 0
            i = 0
            l = len(digits)

            for i in range(l):
                if not digits[i] % 2 == 0:
                    counter += 1
                i += 1
            return counter
        elif a == 0:
            return "Must be > 0!"
    else:
        return "Must be int!"



    # Функция count_odd_num.
    #
    # Принимает натуральное число (целое число > 0).
    # Верните количество нечетных цифр в этом числе.
    # Если число равно 0, то вернуть "Must be > 0!".
    # Если число не целое (не int, а другой тип данных), то вернуть "Must be int!".



def main():
    print(count_odd_num(234561))
    print(count_odd_num(234001))
    print(count_odd_num(0))
    print(count_odd_num(-28861))
    print(count_odd_num('lkjhjhkjh'))


if __name__ == '__main__':
    main()
