def print_symbols_if(str):
    qtty = len(str)
    if qtty == 0:
        return "Empty string!"
    elif qtty > 5:
        print()
    else:
        print('result=', str[1] * qtty)

    """
    Функция print_symbols_if.
    
    Принимает строку.
    
    Если строка нулевой длины, то вывести строку "Empty string!".
    
    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'
    
    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """


def main():
    print_symbols_if('gasdfgdgh')
    print_symbols_if('gasdfgdghfjf')
    print_symbols_if('')
    print_symbols_if('gasd')


if __name__ == '__main__':
    main()
