def print_nth_symbols(str1: str, n: int):
    if n <= 0:
        return 'Must be > 0!'

    if not type(n) == int:
        return 'Must be int!'

    length = len(str1)

    if n > length:
        return ''
    result = ''
    i = 1
    while i*n < length-1:
        result += str1[n * i]
        i += 1
    return result

    """
    Функция print_nth_symbols.
    
    Принимает строку и натуральное число n (целое число > 0).
    Вывести символы с индексом n, n*2, n*3 и так далее.
    Пример: string='123456789qwertyuiop', n = 2 => result='3579wryip'
    
    Если число меньше или равно 0, то вывести строку 'Must be > 0!'.
    Если тип n не int, то вывести строку 'Must be int!'.
    
    Если n больше длины строки, то вывести пустую строку.
    """


def main():
    print(print_nth_symbols('gdgadfs', 2))
    print(print_nth_symbols('1234567890', 2))
    print(print_nth_symbols('0123456789', 3))
    print(print_nth_symbols('g', 2))
    print(print_nth_symbols('gdgadfs', ''))
    print(print_nth_symbols('gdgadfs', 2))


if __name__ == '__main__':
    main()
