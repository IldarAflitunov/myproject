def check_substr(str1: str, str2: 2):
    if str1 == str2:
        return 'Equal'

    if len(str1) == 0 or len(str2) == 0:
        return 'Empty'

    if len(str1) < len(str2) and str1 in str2:
        return 'Include 1 in 2'
    elif len(str1) > len(str2) and str2 in str1:
        return 'Include 2 in 1'
    else:
        return 'not include'

        """
        Функция check_substr.
        
        Принимает две строки.
        Если меньшая по длине строка содержится в большей, то возвращает True,
        иначе False.
        Если строки равны, то False.
        Если одна из строк пустая, то True.
        """


def main():
    print(check_substr("", "fgsg"))
    print(check_substr("fq", "fq sg"))
    print(check_substr("fqsqwrfdvwr", "fqsq"))
    print(check_substr("23r2wgvf", "fgsg"))
    print(check_substr("kjkjlkj", "fgsg"))
    print(check_substr("fqsq", "fgsg"))
    print(check_substr("", "fgsg"))




if __name__ == '__main__':
    main()
