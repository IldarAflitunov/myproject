"""
Функция season.

Принимает 1 аргумент — номер месяца (от 1 до 12).
Возвращает время года, которому этот месяц принадлежит (Winter, Spring, Summer или Fall).
"""