def list_to_dict(lst: list, f_val):
    if type(lst) != list:
        return 'Must be list'

    lst_count = lst.count(f_val)
    i = lst.index(f_val)
    if i:
        is_found = True
    else:
        is_found = False

    res_dict = {f_val: (i, is_found, lst_count)}
    return res_dict
    """
    Функция list_to_dict.
    
    Принимает 2 аргумента: список (с повторяющимися элементами) и значение для поиска.
    
    Возвращает словарь (из входного списка), в котором
    ключ = значение из списка,
    значение = (
    индекс этого элемента в списке,
    найдено ли искомое значение(True | False) в списке,
    количество элементов в словаре.
    )
    """


def main():
    print(list_to_dict(['a', 2, 'fs', 5, 'jem', 'fav', 'sdq', 'jem'], 'jem'))
    print(list_to_dict(['a', 2, 'fs', 5, 'jem', 'fav', 'sdq', 'jem'], 2))
    print(list_to_dict(5, 2))


if __name__ == '__main__':
    main()
