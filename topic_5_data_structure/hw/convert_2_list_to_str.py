def list_to_str(lst: list, divider: str):
    if type(lst) != list:
        return 'lst - Must be list'

    if type(divider) != str:
        return 'divider must be string'
    temp_str = str(lst)

    res_string = [temp_str.split(divider), lst.count(divider) ** 2]
    return res_string
    """
    Функция list_to_str.
    
    Принимает 2 аргумента: список (с повторяющимися элементами) и разделитель (строка).
    
    Возвращает (строку полученную разделением элементов списка разделителем,
    количество разделителей в получившейся строке в квадрате).
    """


def main():
    print(list_to_str(['a', 'b', 'c', 'd', 'e', 'f', 'j'], ' WOW '))
    print(list_to_str(6, ' WOW '))
    print(list_to_str(['a', 'b', 'c', 'd', 'e', 'f', 'j'], 5))


if __name__ == '__main__':
    main()
