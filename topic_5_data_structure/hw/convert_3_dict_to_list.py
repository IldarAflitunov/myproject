def dict_to_list(dictionary: dict):
    if type(dictionary) != dict:
        return 'Must be dict'

    for key in dictionary.keys():
        list_of_keys = [key]

    for value in dictionary.values():
        list_of_values = [value]

    qtty_keys = len(list_of_keys)
    qtty_values = len(list_of_values)

    result_list = [list_of_keys, list_of_values, qtty_keys, qtty_values]
    if len(result_list) == 0:
        return "Result is empty"
    return result_list

    """
        Функция dict_to_list.
        
        Принимает 1 аргумент: словарь (в котором значения повторяются).
        
        Возвращает (список ключей,
        список значений,
        количество элементов в списке ключей,
        количество элементов в списке значений).
        """

    def main():
        print(dict_to_list({'a': 'ab', 'b': 'bc', 'c': 'cd'}))

    if __name__ == '__main__':
        main()
