import pytest

from topic_5_data_structure.hw.convert_1_list_to_dict import list_to_dict

params = [
    (['a', 2, 'fs', 5, 'jem', 'fav', 'sdq', 'jem'], 'jem',{'jem': (4, True, 2)}),
    (['a', 2, 'fs', 5, 'jem', 'fav', 'sdq', 'jem'], 2,{2: (1, True, 1)}),
    (5, 'jem', 'Must be list'),
]

ids = ["(%s) %s (%s) == (%s)" % (lst, f_val, expected) for (lst, f_val, expected) in params]


@pytest.mark.parametrize(argnames="lst, f_val, expected",
                         argvalues=params,
                         ids=ids)
def test_list_to_dict(lst, f_val, expected):
    assert list_to_dict(lst, f_val) == expected
