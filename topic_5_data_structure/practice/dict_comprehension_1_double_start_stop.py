"""
Функция double_start_stop.

Принимает 2 аргумента: числа start, stop.

Возвращает словарь, в котором ключ - это значение от start до stop (не включая),
а значение - удвоенных значение ключа.

Пример: start=3, stop=6, результат {3: 6, 4: 8, 5: 10}.
"""