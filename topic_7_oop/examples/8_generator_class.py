class Generator:
    counter = 0

    def __init__(self, number):
        self.number = number

    def __next__(self):
        if self.counter < self.number:
            result = self.counter
            self.counter += 1
            return result
        else:
            raise StopIteration()

    def __iter__(self):
        return self


newest = Generator(5)
print(list(newest))
print(Generator.counter)
print(newest.counter)
Generator.counter = 9
newest.counter = 7
print(Generator.counter)
print(newest.counter)
# next(newest)
