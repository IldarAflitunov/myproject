class Human:
    def __init__(self, first_name, last_name,  age):
        self.age = age
        self.first_name = first_name
        self.last_name = last_name

    def __get_age__(self):
        return self.age

    def __eq__(self,other):
            pass

instance = Human("Vasilisa")
    print(instance)



"""
Класс Human.

Поля:
age,
first_name,
last_name.

При создании экземпляра инициализировать поля класса.

Создать метод get_age, который возвращает возраст человека.

Перегрузить оператор __eq__, который сравнивает объект человека с другим по атрибутам.

Перегрузить оператор __str__, который возвращает строку в виде “Имя: first_name last_name Возраст: age”.
"""
